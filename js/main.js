
const addQty = (key) => {
    let input = document.getElementById(`quantity-${key}`)
    let qty = parseInt(input.value)
    qty++
    input.value = qty
    SumOrder()
}

const minusQty = (key) => {
    let input = document.getElementById(`quantity-${key}`)
    let qty = input.value
    qty--
    if(qty<1) {
        alert("Số sản phẩm không thể nhỏ hơn 1")
        input.value = 1
    }else{ input.value = qty }
    SumOrder()
}

const del = (key) => {
    let tr = document.getElementById(`item-${key}`)
    tr.remove()
    SumOrder()
}


const SumOrder = () => {
    let items = document.querySelectorAll("tbody tr")
    let totalPrice = 0;
    let totalDiscount = 0;
    let totalTax =0;
    for(i=0; i<items.length; i++){
        let td = items[i].querySelectorAll("td")
        let qty = parseInt(td[2].querySelector("input").value)
        let price = parseInt(td[3].querySelectorAll("span")[1].textContent)
        let discount = td[4].querySelectorAll("span")[1].textContent
        if(discount == "--"){
            discount = 0;
        }else{
            discount = parseInt(discount)
        }

        //  set tax
        let tax = Math.round(qty*price*0.125) 
        td[5].querySelectorAll("span")[1].innerHTML = tax.toFixed(2)

        // tinh total
        let total = qty*price - discount + tax
        td[6].querySelectorAll("span")[1].innerHTML = total.toFixed(2)

        //  tinh totalPrice, totalDiscount, totalTax
        totalPrice += total
        totalDiscount += discount
        totalTax += tax
    }
    document.getElementById('totalPrice').innerHTML = totalPrice.toFixed(2)
    document.getElementById('totalDiscount').innerHTML = totalDiscount.toFixed(2)
    document.getElementById('totalTax').innerHTML = totalTax.toFixed(2)
}

window.onload =  SumOrder()
